import { useState } from 'react';

const Task = ({ id, text, WIP }) => {
  const [code, setCode] = useState('');
  const [result, setResult] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [link, setLink] = useState('');

  const testCode = async () => {
    setIsLoading(true);
    setResult(null);

    try {
      const res = await fetch('http://localhost:8080/result/tests', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json;charset=utf-8',
        },
        body: JSON.stringify({ id, code }),
      });
      const testRes = await res.json();

      setResult(testRes?.testRes);
      setIsLoading(false);
    } catch (e) {
      setIsLoading(false);
      console.log(e);
    }
  };

  const runCode = async () => {
    const res = await fetch('http://localhost:8080/result/link', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
      body: JSON.stringify({ id, code }),
    });
    const link = await res.json();
    setLink(link?.link);
    console.log(link);
  };

  return (
    <div>
      <div
        style={{
          display: 'flex',
          width: '100vw',
          justifyContent: 'space-between',
        }}
      >
        <div>
          <p style={{ textDecoration: WIP ? 'line-through' : 'none' }}>
            {text}
          </p>
          <textarea value={code} onChange={e => setCode(e.target.value)} />
          <p style={{ color: result ? 'green' : 'red' }}>
            {result !== null && (result ? 'passed' : 'failed')}
          </p>
          <button disabled={isLoading} onClick={testCode}>
            {isLoading ? '...' : 'Test'}
          </button>
          <button disabled={isLoading} onClick={runCode}>
            Run
          </button>
        </div>
        {link && (
          <iframe
            src={link}
            style={{
              width: '500px',
              height: '500px',
              border: 0,
              borderRadius: '4px',
              overflow: 'hidden',
            }}
            sandbox="allow-forms allow-modals allow-popups allow-presentation allow-same-origin allow-scripts"
          />
        )}
      </div>

      <hr />
    </div>
  );
};

export default Task;
