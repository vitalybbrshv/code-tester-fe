import React from 'react';
import Task from './Task';

const initTasks = [
  { id: 0, text: 'TEST', WIP: false },
  {
    id: 1,
    text: 'cоздать простейшую форму с логином и паролем и кнопкой войти',
    WIP: false,
  },
  {
    id: 2,
    text:
      'должен отправиться запрос например через fetch api - на любой адрес например google.com',
    WIP: true,
  },
  {
    id: 3,
    text:
      'написать компонент App, который рендерит список (ul) фруктов (Apple, Banana, Orange)',
    WIP: false,
  },
];

const App = () => (
  <div className="App">
    {initTasks.map(task => (
      <Task key={task.id} {...task} />
    ))}
    <div style={{ padding: '100px' }}>
      {'export const fn = (a) => a+2'}
      <hr />
      {'<input type="email"/>'}
      <br />
      {' <input type="password" />'}
      <br />
      {'<button type="submit"/>'}
      <hr />
      {'const App = () => ('}
      <br />
      {'  <ul>'}
      <br />
      {'    <li>Apple</li>'}
      <br />
      {'    <li>Banana</li>'}
      <br />
      {'    <li>Orange</li>'}
      <br />
      {'  </ul>'}
      <br />
      {');'}
    </div>
  </div>
);

export default App;
